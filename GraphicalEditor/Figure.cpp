#include "Figure.h"

Figure::Figure() : scope({})
{
}

Figure::Figure(std::vector<PrimitiveBase*>& vector) : scope(vector)
{
}

Figure::~Figure()
{
    for (auto p : scope)
    {
        delete p;
    }
}

void Figure::addItem(PrimitiveBase* item)
{
    scope.push_back(item);
}

std::vector<PrimitiveBase*> Figure::getScope() const
{
    return scope;
}

double Figure::calculateTotalArea() const
{
    double totalArea = 0;
    for (auto item : scope)
    {
        totalArea += item->calculateArea();
    }

    return totalArea;
}

double Figure::calculatePaintVolume(double paintUsage) const
{
    double paintVolume = 0;
    for (auto item : scope)
    {
        paintVolume += item->calculateLength();
    }

    return paintVolume * paintUsage;
}



