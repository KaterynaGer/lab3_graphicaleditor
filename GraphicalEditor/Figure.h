#pragma once
#include <vector>
#include "PrimitiveBase.h"
class Figure
{
public:
    Figure();
    Figure(std::vector<PrimitiveBase*>&);
    ~Figure();
    void addItem(PrimitiveBase*);
    std::vector<PrimitiveBase*> getScope() const;
    double calculatePaintVolume(double paintUsage = 12) const;
    double calculateTotalArea() const;
private:
    std::vector<PrimitiveBase*> scope;
};

