#include "Line.h"
#include <cmath>

Line::Line() : PrimitiveBase(Point()), endPoint(Point())
{
}

Line::Line(Point point1, Point point2) : PrimitiveBase(point1), endPoint(point2)
{
}

Point Line::getEndPoint() const
{
    return endPoint;
}

void Line::setEndPoint(Point point)
{
    endPoint = point;
}

double Line::calculateArea() const
{
    return 0.0;
}

double Line::calculateLength() const
{
    return Point::calcDistance(basePoint, endPoint);
}

