#pragma once
#include "PrimitiveBase.h"
#include "Point.h"

class Sector :
    public PrimitiveBase
{
public:
    Sector();
    Sector(Point, Point, double);
    //Sector(Point, Point, Point);

    Point getStartPoint() const;
    double getAngle() const;
    double getRadius() const;
    void setAngle(double);
    void setStartPoint(Point);
    double calculateArea() const override;
    double calculateLength() const override;

private:
    Point startPoint;
    double angle;
};

