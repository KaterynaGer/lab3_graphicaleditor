#include "Circle.h"
#include "Polygon.h"
#include <cmath>
#include "MathUtils.h"

Circle::Circle() : PrimitiveBase(Point()), radius(0.0)
{
}

Circle::Circle(Point point, double radius) : PrimitiveBase(point), radius(radius)
{
}

Circle::Circle(Point point1, Point point2) : PrimitiveBase(point1)
{
    radius = Point::calcDistance(point1, point2);
}

Circle::Circle(const Polygon& polygon, bool type) : PrimitiveBase(polygon.getBasePoint())
{
	if (type) //inscribed polygon
	{
		radius = polygon.getRadius() * (std::cos(mathUtils::PI / polygon.getSegmentNumber()));
	}
	else
	{
		radius = polygon.getRadius();
	}
}


double Circle::getRadius() const
{
	return radius;
}

void Circle::setRadius(double r)
{
    radius = r;
}


double Circle::calculateArea() const
{
	return  mathUtils::PI * radius * radius;
}

double Circle::calculateLength() const
{
	return  2 * mathUtils::PI * radius;
}


