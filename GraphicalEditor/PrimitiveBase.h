#pragma once
#include "Point.h"

class PrimitiveBase
{
public:
    PrimitiveBase(Point);
    Point getBasePoint() const;
    void setBasePoint(Point);
    virtual double calculateLength() const = 0;
    virtual double calculateArea() const = 0;

protected:
    Point basePoint;
};