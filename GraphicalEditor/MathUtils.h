#pragma once

namespace mathUtils
{
    const  double PI = 3.141592653589793238463;
    const double EPSILON = 1e-8;
    
    bool areSame(double a, double b);

}

