#include <cmath>
#include "Point.h"

Point::Point() : x(0), y(0)
{}

Point::Point(double x, double y) : x(x), y(y)
{
}

double Point::getX() const
{
    return x;
}

double Point::getY() const
{
    return y;
}

double Point::calcDistance(Point point1, Point point2)
{
    double deltaX = point1.getX() - point2.getX();
    double deltaY = point1.getY() - point2.getY();
    return std::sqrt(deltaX * deltaX + deltaY * deltaY);
}

