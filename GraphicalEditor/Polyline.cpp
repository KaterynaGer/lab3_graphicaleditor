#include <cmath>
#include "Polyline.h"

Polyline::Polyline() :PrimitiveBase({}), points({})
{
}

Polyline::Polyline(std::vector<Point> points) : PrimitiveBase(points[0]), points(points)
{

}

std::vector<Point> Polyline::getPoints() const
{
    return points;
}

void Polyline::setPoints(const std::vector<Point>& p)
{
    points = p;
}

double Polyline::calculateLength() const
{
    double length = Point::calcDistance(basePoint, points.back());

    for (auto it = points.begin(); it != points.end() - 1; ++it)
    {
        length += Point::calcDistance(*it, *(it + 1));
    }

    return length;
}

double Polyline::calculateArea() const
{
    //TODO
    double area = 0.0;
    int size = points.size();
    for (int i = 0; i < size; ++i)
    {
        int j = (i + 1) % size;
        area += 0.5 * (points[i].getX() * points[j].getY() - points[j].getX() * points[i].getY());
    }

    return fabs(area);
}


