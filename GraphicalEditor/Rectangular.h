#pragma once
#include "PrimitiveBase.h"
#include "Point.h"

class Rectangular :
    public PrimitiveBase
{
public:
    Rectangular();
    Rectangular(Point, double, double, double);
    double getHeight() const;
    double getWidth() const;
    double getAngle() const;
    void setHeight(double);
    void setWidth(double);
    void setAngle(double);


    double calculateLength() const override;
    double calculateArea() const override;
private:
    double height;
    double width;
    double angle;
};

