#include "PrimitiveBase.h"

PrimitiveBase::PrimitiveBase(Point point1) : basePoint{ point1 }
{
}

Point PrimitiveBase::getBasePoint() const
{
    return basePoint;
}

void PrimitiveBase::setBasePoint(Point point)
{
    basePoint = point;
}



