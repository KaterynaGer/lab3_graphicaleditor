#pragma once
#include "PrimitiveBase.h"

class Polygon; //forward declaration

class Circle :
    public PrimitiveBase
{
public:
    Circle();
    Circle(Point, double);
    Circle(Point, Point);
    Circle(const Polygon&, bool);
    double getRadius() const;
    void setRadius(double);
    double calculateArea() const override;
    double calculateLength() const override;

private:
    double radius;
};

