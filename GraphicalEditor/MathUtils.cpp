#pragma once
#include "MathUtils.h"
#include <cmath>

namespace mathUtils
{
    bool areSame(double a, double b)
    {
        return fabs(a - b) < mathUtils::EPSILON;
    }
}
