#include <cmath>
#include "Sector.h"

Sector::Sector() : PrimitiveBase({}), startPoint({}), angle(0.0)
{
}

Sector::Sector(Point point1, Point point2, double fi)
    : PrimitiveBase(point1), startPoint(point2), angle(fi)
{
}

//Sector::Sector(Point point1, Point point2, Point point3)
//    :PrimitiveBase(point1), startPoint(point2)
//{
//    angle = 2 * asin(Point::calcDistance(point2, point3) * 0.5 / Point::calcDistance(point1, point2));
//}

Point Sector::getStartPoint() const
{
    return startPoint;
}

double Sector::getAngle() const
{
    return angle;
}

double Sector::getRadius() const
{
    return Point::calcDistance(basePoint, startPoint);
}

void Sector::setAngle(double fi)
{
    angle = fi;
}

void Sector::setStartPoint(Point point)
{
    startPoint = point;
}

double Sector::calculateArea() const
{
    return getRadius() * getRadius() * angle / 2;
}

double Sector::calculateLength() const
{
    return 2 * getRadius() + getRadius() * angle;
}





