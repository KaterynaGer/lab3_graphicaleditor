#include <iostream>
#include <vector>

#include "Polyline.h"
#include "Circle.h"
#include "Line.h"
#include "Point.h"
#include "Polygon.h"
#include "Rectangular.h"
#include "Sector.h"

#include "Figure.h"

int main()
{
    Figure figure = {};

    PrimitiveBase* polyline1 = new Polyline({ {1,1},{10,10},{15,4},{10,-2},{7,3} });
    figure.addItem(polyline1);
    PrimitiveBase* circle1 = new Circle({ 1,1 }, 90);
    figure.addItem(circle1);
    PrimitiveBase* line1 = new Line({ 45,-67.5 }, { -10,93 });
    figure.addItem(line1);
    PrimitiveBase* polygon1 = new Polygon(dynamic_cast<Circle&>(*circle1), 7, 2.5, true);
    figure.addItem(polygon1);
    PrimitiveBase* rectangular1 = new Rectangular({ -8,56 }, 45, 23, 0);
    figure.addItem(rectangular1);
    PrimitiveBase* sector1 = new Sector({ 5,6 }, { 5,16 }, 3.14);
    figure.addItem(sector1);

    double paint = figure.calculatePaintVolume();
    double area = figure.calculateTotalArea();
    return 0;
}