#pragma once
#include "PrimitiveBase.h"

class Line :
    public PrimitiveBase
{
public:
    Line();
    Line(Point, Point);
    Point getEndPoint() const;
    void setEndPoint(Point);
    double calculateArea() const override;
    double calculateLength() const override;

private:
    Point endPoint;
};

