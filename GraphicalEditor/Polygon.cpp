﻿#include "Polygon.h"
#include <cmath>
#include <stdexcept>
#include "MathUtils.h"

Polygon::Polygon() : PrimitiveBase(Point()), startPoint(Point()), segmentsNumber(5)
{
}

Polygon::Polygon(Point point1, Point point2, int number)
    : PrimitiveBase(point1), startPoint(point2), segmentsNumber(number)
{
    if (number < 5 || number > 8)
    {
        throw std::out_of_range("Number of segments must be less than 9 and more than 4");
    }
}

Polygon::Polygon(const Circle& circle, int number, double startPointAngle, bool type)
    : PrimitiveBase(circle.getBasePoint())
{
    if (number < 5 || number > 8)
    {
        throw std::out_of_range("Number of segments must be less than 9 and more than 4");
    }

    segmentsNumber = number;
    double radius;
    if (type) //inscribed polygon
    {
        radius = circle.getRadius();
    }
    else
    {
        radius = circle.getRadius() / (std::cos(mathUtils::PI / number));
    }

    while (startPointAngle > mathUtils::PI * 2)
    {
        startPointAngle -= mathUtils::PI * 2;
    }

    startPoint = { radius * std::cos(startPointAngle),radius * std::sin(startPointAngle) };
}

int Polygon::getSegmentNumber() const
{
    return segmentsNumber;
}

void Polygon::setSegmentsNumber(int number)
{
    if (number < 5 || number > 8)
    {
        throw std::out_of_range("Number of segments must be less than 9 and more than 4");
    }

    segmentsNumber = number;
}

Point Polygon::getStartPoint() const
{
    return startPoint;
}

void Polygon::setStartPoint(Point point)
{
    startPoint = point;
}

double Polygon::getRadius() const
{
    return Point::calcDistance(basePoint, startPoint);
}

double Polygon::getEdgeLength() const
{
    return  getRadius() * 2 * std::sin(mathUtils::PI / segmentsNumber);
}

bool Polygon::isInscribed(const Circle& circle) const //Inscribed polygon
{
    return mathUtils::areSame(getRadius(), circle.getRadius());
}

bool Polygon::isCircumscribed(const Circle& circle) const  //Circumscribed polygon
{
    return mathUtils::areSame(circle.getRadius(), getRadius() * std::cos(mathUtils::PI / segmentsNumber));
}

double Polygon::calculateArea() const
{
    double radius = getRadius();
    double edgeLength = getEdgeLength();
    return sqrt(radius * radius - edgeLength * edgeLength / 4) * edgeLength / 2 * segmentsNumber;
}

double Polygon::calculateLength() const
{
    return segmentsNumber * getEdgeLength();
}


