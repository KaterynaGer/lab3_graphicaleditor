#pragma once
#include <vector>
#include "Point.h"
#include "PrimitiveBase.h"
class Polyline :
    public PrimitiveBase
{
public:
    Polyline();
    Polyline(std::vector<Point>);
    std::vector<Point> getPoints() const;
    void setPoints(const std::vector<Point>&);
    double calculateLength() const override;
    double calculateArea() const override;
private:
    std::vector<Point> points;
};

