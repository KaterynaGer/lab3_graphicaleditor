﻿#pragma once

#include "PrimitiveBase.h"
#include "Circle.h"

class Polygon : public PrimitiveBase
{
public:
    Polygon();
    Polygon(Point, Point, int);
    Polygon(const Circle&, int, double, bool);

    int getSegmentNumber() const;
    void setSegmentsNumber(int);
    Point getStartPoint() const;
    void setStartPoint(Point);
    double getRadius() const;
    double getEdgeLength() const;
    bool isInscribed(const Circle&) const;
    bool isCircumscribed(const Circle&) const;
    double calculateArea() const override;
    double calculateLength() const override;

private:
    Point startPoint;
    int segmentsNumber;
};
