#pragma once
class Point
{
public:
    Point();
    Point(double, double);
    double getX() const;
    double getY() const;
    static double calcDistance(Point, Point);

private:
    double x;
    double y;
};

