#include "Rectangular.h"

Rectangular::Rectangular() :PrimitiveBase({}), height(0.0), width(0.0), angle(0.0)
{
}

Rectangular::Rectangular(Point point, double length, double width, double fi)
    : PrimitiveBase(point), height(length), width(width), angle(fi)
{
}

double Rectangular::getHeight() const
{
    return height;
}

double Rectangular::getWidth() const
{
    return width;
}

double Rectangular::getAngle() const
{
    return angle;
}

void Rectangular::setHeight(double h)
{
    height = h;
}

void Rectangular::setWidth(double w)
{
    width = w;
}

void Rectangular::setAngle(double fi)
{
    angle = fi;
}

double Rectangular::calculateLength() const
{
    return 2 * (height + width);
}

double Rectangular::calculateArea() const
{
    return height * width;
}

